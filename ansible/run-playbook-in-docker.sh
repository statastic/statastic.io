#!/bin/bash

. ./set-stage-env-vars.sh

export SSH_PRIVATE_KEY="$(kwallet-query kdewallet -f statastic -r ssh_key)"
export ANSIBLE_VAULT_PASSWORD=$(kwallet-query kdewallet -f statastic -r ansible-vault)

docker run -v $(pwd):/ansible \
  -eSSH_PRIVATE_KEY="$SSH_PRIVATE_KEY" \
  -eANSIBLE_VAULT_PASSWORD \
  -eSSH_SERVER_HOSTKEYS \
  registry.gitlab.com/statastic/statastic-ansible \
  --vault-password-file /ansible/vault-password.sh \
  -i /ansible/inventory.yml \
  /ansible/site.yml