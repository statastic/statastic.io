How to Set Up Local Environment with Vagrant
============================================

Prerequisites
-----------------
* ansible-galaxy collection install community.general

Start Vagrant box
-----------------
* git checkout test
* cd PROJECT_DIR/vagrant
* vagrant up
* add entries to /etc/hosts on the host with box-ip, e.g."192.168.56.11 statastic.test jenkins.statastic.test keycloak.statastic.test wildfly.statastic.test"
```
192.168.56.23  statastic.test jenkins.statastic.test keycloak.statastic.test wildfly.statastic.test wiki.statastic.test couchdb.statastic.test influxdb.statastic.test
```

Run Ansible Playbook
--------------------
* cd PROJECT_DIR/ansible
* export ANSIBLE_VAULT_PASSWORD=$(kwallet-query kdewallet -f statastic -r ansible-vault)
* ./run-playbook

Create Access Token in InfluxDB for Jenkins
----------------------------
The token to access InfluxDB from Jenkins cannot be created automatically. It has to be created and then configured manually.
* create token (todo...)
* configure secret (todo...)
* ./run-playbook
